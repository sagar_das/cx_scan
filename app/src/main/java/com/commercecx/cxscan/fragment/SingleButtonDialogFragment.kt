package com.commercecx.cxscan.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment


@SuppressLint("ValidFragment")
class SingleButtonDialogFragment(
    protected var positiveButtonTitle: Int,
    protected var message: String, protected var title: String
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
            .setTitle(title)
            .setCancelable(isCancelable)
            .setMessage(message)
            .setPositiveButton(
                positiveButtonTitle,
                DialogInterface.OnClickListener { dialog, which -> })
        return builder.create()
    }

}