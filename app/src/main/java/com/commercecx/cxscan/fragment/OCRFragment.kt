package com.commercecx.cxscan.fragment

import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.os.Bundle
import android.os.ParcelFileDescriptor
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.commercecx.cxscan.R
import com.commercecx.cxscan.utils.BitmapUtils
import com.commercecx.cxscan.utils.CloudVisionUtils
import com.commercecx.cxscan.utils.PDF_ARG_KEY
import kotlinx.android.synthetic.main.fragment_o_c_r.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File


class OCRFragment : Fragment() {

    var baseView: View? = null
    var ocrBitmap: Bitmap? = null
    var coroutineScope = CoroutineScope(Dispatchers.Main)
    var ocrFragment: OCRFragment? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        baseView = inflater.inflate(R.layout.fragment_o_c_r, container, false)
        baseView!!.tvHeader.visibility = View.GONE
        val file = requireArguments().getSerializable(PDF_ARG_KEY) as File
        convertFileToBitmap(file)
        baseView!!.tvOCRResult.text = "Fetching results from Google OCR"
        initiateOCR()
        ocrFragment = this
        return baseView



    }

    fun convertFileToBitmap(file: File){

        val parcelFileDescriptor = ParcelFileDescriptor.open(file,ParcelFileDescriptor.MODE_READ_ONLY)
        val pdfRenderer = PdfRenderer(parcelFileDescriptor)
        val page = pdfRenderer.openPage(0)
        ocrBitmap = Bitmap.createBitmap(page.width,page.height,Bitmap.Config.ARGB_8888)
        page.render(ocrBitmap!!, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
        baseView!!.ivSample.setImageBitmap(ocrBitmap)


    }

    fun initiateOCR(){

        val scaledOcrBitmap = BitmapUtils.scaleBitmapDown(ocrBitmap!!,300)
        val annotationRequest = CloudVisionUtils.prepareAnnotationRequest(scaledOcrBitmap!!, requireActivity().packageName, requireActivity().packageManager)

        coroutineScope.launch {
            CloudVisionUtils.connectToCloudVision(ocrFragment!!,requireContext(),annotationRequest!!)
        }


    }

    fun setResultText(resultText: String){
        baseView!!.tvHeader.visibility = View.VISIBLE
        val results = resultText.split("\n\n")
        baseView!!.tvOCRResult.text = results[0]
    }


}
