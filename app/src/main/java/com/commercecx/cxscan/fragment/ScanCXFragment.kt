package com.commercecx.cxscan.fragment

import android.content.pm.PackageManager
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment

import com.commercecx.cxscan.R
import com.commercecx.cxscan.utils.PermissionsUtil
import com.commercecx.cxscan.views.ScanRectangle
import com.pengke.paper.scanner.scan.IScanView
import com.pengke.paper.scanner.scan.ScanPresenter
import kotlinx.android.synthetic.main.fragment_scan_c_x.view.*
import org.opencv.android.OpenCVLoader

/**
 * A simple [Fragment] subclass.
 */
class ScanCXFragment : BaseFragment(), IScanView.Proxy {

    private val REQUEST_CAMERA_PERMISSION = 0
    private val EXIT_TIME = 2000

    private lateinit var mPresenter: ScanPresenter
    private var latestBackPressTime: Long = 0

    private val TAG = this.javaClass.simpleName


    internal var callback: ScanCXFragmentButtonListener? = null

    fun setOnScanCXFragmentButtonListener(callback: ScanCXFragmentButtonListener){
        this.callback = callback
    }


    interface ScanCXFragmentButtonListener{
        fun onClickScanBtn()
    }


    override fun provideContentViewId(): Int = R.layout.fragment_scan_c_x

    override fun initPresenter() {
        mPresenter = ScanPresenter(requireContext(), this,callback)
    }

    override fun prepare() {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "loading opencv error, exit")
            requireActivity().finish()
        }

        if(!PermissionsUtil.hasPermissions(requireContext()))
        {
            PermissionsUtil.requestPermissions(requireActivity())
        }

        getFragmentView()!!.shut.setOnClickListener {
            mPresenter.shut()
        }
        latestBackPressTime = System.currentTimeMillis()
    }


    override fun onStart() {
        super.onStart()
        mPresenter.start()
    }

    override fun onStop() {
        super.onStop()
        mPresenter.stop()
    }

    override fun exit() {
        requireActivity().finish()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION
            && (grantResults[permissions.indexOf(android.Manifest.permission.CAMERA)] == PackageManager.PERMISSION_GRANTED)) {
            showMessage(R.string.camera_grant)
            mPresenter.initCamera()
            mPresenter.updateCamera()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun getDisplay(): Display = requireActivity().windowManager.defaultDisplay

    override fun getSurfaceView(): SurfaceView = getFragmentView()!!.surface

    override fun getPaperRect(): ScanRectangle = getFragmentView()!!.paper_rect
}
