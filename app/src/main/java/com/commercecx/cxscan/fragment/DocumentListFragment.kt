package com.commercecx.cxscan.fragment

import android.R.attr.category
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.commercecx.cxscan.R
import com.commercecx.cxscan.activity.ScanCXActivity
import com.commercecx.cxscan.adapters.DocumentAdapter
import com.commercecx.cxscan.model.Document
import com.commercecx.cxscan.utils.*
import com.commercecx.cxscan.viewmodel.DocumentViewModel
import kotlinx.android.synthetic.main.fragment_document_list.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class DocumentListFragment : Fragment(),View.OnClickListener {

    private val documentViewModel: DocumentViewModel by viewModels {
        InjectorUtils.provideDocumentViewModelFactory(requireContext())
    }

    val scannedBitmaps: MutableList<Uri> = mutableListOf()
    var documentAdapter: DocumentAdapter? = null

    interface DocumenListCallback{
        fun onOCRClick(file: File)
    }

    var mCallback: DocumenListCallback? = null

    fun setCallbackListener(callback: DocumenListCallback){
        mCallback = callback
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_document_list, container, false)

        view.btnOCR.setOnClickListener(this)

        if(!PermissionsUtil.hasPermissions(requireContext()))
        {
            PermissionsUtil.requestPermissions(requireActivity())
        }
        else{

        }

        FileIO.mkdir(BASE_STORAGE_DATA)
        FileIO.mkdir(BASE_STAGING_DATA)

        documentAdapter = DocumentAdapter(documentViewModel,requireContext(),this)
        view.rvDocuments.adapter = documentAdapter

        val liveData = documentViewModel.getAllDocs()
        liveData.observe(viewLifecycleOwner,
            Observer<List<Document>?> { documents ->
                if (documents!!.size > 0) {
                    view.llEmpty.setVisibility(View.GONE)
                } else {
                    view.llEmpty.setVisibility(View.VISIBLE)
                }

                documentAdapter!!.setData(documents.toMutableList())
            })
        val linearLayoutManager = LinearLayoutManager(requireContext())
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        view.rvDocuments.setLayoutManager(linearLayoutManager)

        val dividerItemDecoration =
            DividerItemDecoration(view.rvDocuments.context, linearLayoutManager.orientation)
        view.rvDocuments.addItemDecoration(dividerItemDecoration)
        return view
    }


    override fun onClick(v: View?) {

        if(v!!.id == R.id.btnOCR){
            scannedBitmaps.clear()
            //Toast.makeText(requireContext(),"Clicked",Toast.LENGTH_SHORT).show()
//            val intent = Intent(requireContext(), ScanActivity::class.java).apply {
//                putExtra(OPEN_INTENT_PREFERENCE, OPEN_CAMERA)
//            }
//            startActivityForResult(intent, START_CAMERA_REQUEST_CODE);

            val intent = Intent(requireContext(),ScanCXActivity::class.java)
            startActivityForResult(intent, START_CAMERA_REQUEST_CODE);


        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PERMISSIONS_REQ_CODE && resultCode == Activity.RESULT_OK){

        }
        else if(( requestCode == PICKFILE_REQUEST_CODE || requestCode == START_CAMERA_REQUEST_CODE ) &&
            resultCode == Activity.RESULT_OK)
        {
                Toast.makeText(requireContext(),"Photo is clicked",Toast.LENGTH_SHORT).show()

            val uri: Uri? = data!!.extras!!.getParcelable(SCANNED_RESULT)
            val doScanMore = data!!.extras!!.getBoolean(SCAN_MORE)

            try {
                val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
                saveBitmap(bitmap, doScanMore)
                if (doScanMore) {
//                    scannedBitmaps.add(uri!!)
//                    val intent = Intent(requireContext(), ScanActivity::class.java)
//                    intent.putExtra(OPEN_INTENT_PREFERENCE, OPEN_CAMERA)
//                    startActivityForResult(intent, START_CAMERA_REQUEST_CODE)
                }

                //getContentResolver().delete(uri, null, null);
            } catch (e: IOException) {
                e.printStackTrace()
            }


        }
    }

    fun saveBitmap(bitmap: Bitmap, addMore: Boolean){

        try {
            val baseDirectory: String = if (addMore) BASE_STAGING_DATA else BASE_STORAGE_DATA
            val sd: File = Environment.getExternalStorageDirectory()

            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy_hh-mm-ss")
            val timestamp: String = simpleDateFormat.format(Date())
            val pdfWriter = PDFWriter()
            val stagingDirPath: String = BASE_STAGING_DATA

            val stagingFiles: List<File> =
                FileIO.getAllFiles(stagingDirPath)
            for (stagedFile in stagingFiles) {
                pdfWriter.addFile(stagedFile)
            }
            pdfWriter.addBitmap(bitmap)
            val filename = "SCANNED_" + timestamp.toString() + ".pdf"
            FileIO.writeFile(baseDirectory, filename, object : FileOperationCallback {
                override fun write(out: FileOutputStream) {
                    try {
                        pdfWriter.write(out)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            })
            documentAdapter!!.notifyDataSetChanged()
            FileIO.clearDirectory(stagingDirPath)
            val simpleDateFormatView = SimpleDateFormat("dd-MM-yyyy hh:mm")
            val timestampView: String = simpleDateFormatView.format(Date())
            val newDocument = Document(name = filename,category = category.toString(), filePath = filename, pageCount = pdfWriter.getPageCount(), scanned = timestampView)
            documentViewModel.insertDoc(newDocument)
            pdfWriter.close()
            bitmap.recycle()
            System.gc()
        } catch (ioe: IOException) {
            ioe.printStackTrace()
        }

    }

    fun onOCRClick(file: File){
        //Toast.makeText(requireContext(),"This is clicked",Toast.LENGTH_SHORT).show()
        mCallback!!.onOCRClick(file)


    }

}
