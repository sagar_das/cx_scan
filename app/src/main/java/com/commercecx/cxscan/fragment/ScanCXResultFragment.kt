package com.commercecx.cxscan.fragment

import androidx.fragment.app.Fragment
import android.widget.ImageView

import com.commercecx.cxscan.R
import com.commercecx.cxscan.views.ScanRectangle
import com.pengke.paper.scanner.crop.ICropView
import com.pengke.paper.scanner.crop.ResultPresenter
import kotlinx.android.synthetic.main.fragment_scan_c_x_result.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
class ScanCXResultFragment : BaseFragment(), ICropView.PaperDetectionImpl {

    private lateinit var mPresenter: ResultPresenter

    private val resultScope = CoroutineScope(Dispatchers.Main)

    override fun prepare() {
        getFragmentView()!!.crop.setOnClickListener { mPresenter.crop() }
        getFragmentView()!!.enhance.setOnClickListener { mPresenter.enhance() }
        getFragmentView()!!.save.setOnClickListener { resultScope.launch {
            mPresenter.save()
        } }
        getFragmentView()!!.btnRotateLeft.setOnClickListener{ mPresenter.rotateLeftBitmap()}
        getFragmentView()!!.btnRotateRight.setOnClickListener{ mPresenter.rotateRightBitmap()}
    }

    override fun provideContentViewId(): Int = R.layout.fragment_scan_c_x_result


    override fun initPresenter() {
        mPresenter = ResultPresenter(requireContext(), requireActivity(), this)
    }

    override fun getPaper(): ImageView = getFragmentView()!!.paper

    override fun getPaperRect(): ScanRectangle = getFragmentView()!!.paper_rect

    override fun getCroppedPaper(): ImageView = getFragmentView()!!.picture_cropped

}
