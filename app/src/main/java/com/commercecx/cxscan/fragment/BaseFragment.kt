package com.commercecx.cxscan.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment(){

    var fragment_view: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        fragment_view = inflater.inflate(provideContentViewId(), container, false)
        initPresenter()
        prepare()

        return fragment_view
    }

    fun getFragmentView(): View?{
        return fragment_view
    }


    fun showMessage(id: Int) {
        Toast.makeText(requireContext(), id, Toast.LENGTH_SHORT).show();
    }

    abstract fun provideContentViewId(): Int

    abstract fun initPresenter()

    abstract fun prepare()
}