package com.commercecx.cxscan.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import com.commercecx.cxscan.model.Document
import com.commercecx.cxscan.model.dao.DocumentDao
import com.commercecx.cxscan.model.instances.DocumentInstance
import java.util.concurrent.ExecutorService

class DocumentViewModel (documentInstance: DocumentInstance) : ViewModel(){


    val documentInstance = documentInstance

    fun getAllDocs(): LiveData<List<Document>>{
            return documentInstance.getAllDocs()
    }

    fun getDocsByName(nameKey: String): LiveData<List<Document>>{
        return documentInstance.getDocsByName(nameKey)
    }

    fun getDocById(idKey: Int): Document{
        return documentInstance.getDocsById(idKey)
    }

    fun insertDoc(document: Document){

        viewModelScope.launch {
            documentInstance.insertDoc(document)
        }
    }

    fun insertAllDocs(documents: List<Document>){

        viewModelScope.launch {
            documentInstance.insertAllDoc(documents)
        }
    }

    fun updatetDoc(document: Document){

        viewModelScope.launch {
            documentInstance.updateDoc(document)
        }
    }

    fun deleteDoc(document: Document){

        viewModelScope.launch {
            documentInstance.deleteDoc(document)
        }
    }



}