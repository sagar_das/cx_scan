package com.commercecx.cxscan.activity

import android.content.pm.PackageManager
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.Display
import android.view.SurfaceView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.commercecx.cxscan.R
import com.commercecx.cxscan.fragment.DocumentListFragment
import com.commercecx.cxscan.fragment.ScanCXFragment
import com.commercecx.cxscan.fragment.ScanCXResultFragment
import com.commercecx.cxscan.utils.PermissionsUtil
import com.commercecx.cxscan.views.ScanRectangle
import com.pengke.paper.scanner.scan.IScanView
import com.pengke.paper.scanner.scan.ScanPresenter
import kotlinx.android.synthetic.main.activity_scan_c_x.*
import org.opencv.android.OpenCVLoader

class ScanCXActivity : AppCompatActivity(), ScanCXFragment.ScanCXFragmentButtonListener {

    val TAG = this.javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_c_x)
        initiateScanFragment()
    }

    override fun onAttachFragment(fragment: Fragment) {
        if(fragment is ScanCXFragment){
            fragment.setOnScanCXFragmentButtonListener(this)
        }
    }

    private fun initiateScanFragment() {

        val fragment = ScanCXFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(
            R.id.flScan,
            fragment,
            TAG
        )
        ft.commit()
    }


    private fun initiateResultFragment() {

        val fragment = ScanCXResultFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(
            R.id.flScan,
            fragment,
            TAG
        )
        ft.commit()
    }

    override fun onClickScanBtn() {
        initiateResultFragment()
    }


}
