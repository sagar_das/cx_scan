package com.commercecx.cxscan.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.commercecx.cxscan.R
import com.commercecx.cxscan.model.ScanDatabase


class SplashActivity : AppCompatActivity() {

    companion object{
        val SPLASH_TIME_OUT = 3000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        ScanDatabase.getInstance(applicationContext)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(Runnable {
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        },
            SPLASH_TIME_OUT
        )
    }

    private fun restorePrefData(): Boolean {
        val pref = applicationContext.getSharedPreferences(
            "cxPrefs",
            Context.MODE_PRIVATE
        )
        return pref.getBoolean("isIntroOpnend", false)
    }
}
