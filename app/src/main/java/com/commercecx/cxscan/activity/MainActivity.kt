package com.commercecx.cxscan.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.commercecx.cxscan.R
import com.commercecx.cxscan.fragment.DocumentListFragment
import com.commercecx.cxscan.fragment.OCRFragment
import com.commercecx.cxscan.utils.PDF_ARG_KEY
import java.io.File

class MainActivity : AppCompatActivity(), DocumentListFragment.DocumenListCallback{

    companion object{
        val TAG = "Fragment"

    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onDocumentListClick()


    }

    override fun onAttachFragment(fragment: Fragment) {
        if(fragment is DocumentListFragment){
            fragment.setCallbackListener(this)
        }
    }

    private fun onDocumentListClick() {

        val fragment = DocumentListFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(
            R.id.frame_layout_fl,
            fragment,
            TAG
        )
        ft.commit()
    }

    override fun onOCRClick(file: File) {

        val fragment = OCRFragment()
        val ft = supportFragmentManager.beginTransaction()
        val bundle: Bundle = Bundle()
        bundle.putSerializable(PDF_ARG_KEY, file)
        fragment.arguments = bundle
        ft.replace(
            R.id.frame_layout_fl,
            fragment,
            TAG
        )
        ft.commit()
    }


}
