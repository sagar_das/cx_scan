package com.commercecx.cxscan.adapters


import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Environment
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.commercecx.cxscan.R
import com.commercecx.cxscan.fragment.DocumentListFragment
import com.commercecx.cxscan.model.Document
import com.commercecx.cxscan.utils.BASE_STORAGE_DATA
import com.commercecx.cxscan.viewmodel.DocumentViewModel
import java.io.File


class DocumentAdapter( viewModel: DocumentViewModel, context: Context, doclistFrag: DocumentListFragment ) : RecyclerView.Adapter<DocumentAdapter.DocumentViewHolder>(){

    var context: Context? = null
    var viewModel: DocumentViewModel? = null
    protected var mActionMode: ActionMode? = null
    var doclistFrag: DocumentListFragment? = null

    var multiSelect = false
    var documentList: MutableList<Document> = mutableListOf()
    var selectedItems: MutableList<Document> = mutableListOf()

    init {
        this.context = context
        this.viewModel = viewModel
        this.doclistFrag = doclistFrag
    }



    private val actionModeCallbacks: ActionMode.Callback =
        object : ActionMode.Callback {
            override fun onCreateActionMode(
                mode: ActionMode?,
                menu: Menu?
            ): Boolean {
                multiSelect = true
                return true
            }

            override fun onPrepareActionMode(
                mode: ActionMode,
                menu: Menu
            ): Boolean {
                return if (selectedItems.isEmpty() || selectedItems.size == 1) {
                    val inflater = mode.menuInflater
                    menu.clear()
                    inflater.inflate(R.menu.single_select_menu, menu)
                    mode.title = "1 Selected"
                    true
                } else {
                    val inflater = mode.menuInflater
                    menu.clear()
                    inflater.inflate(R.menu.multi_select_menu, menu)
                    mode.title = selectedItems.size.toString() + " Selected"
                    true
                }
            }

            override fun onActionItemClicked(
                mode: ActionMode,
                item: MenuItem
            ): Boolean {
                return when (item.getItemId()) {
                    R.id.menu_delete -> {
                        for (documentItem in selectedItems) {
                            val baseDirectory = BASE_STORAGE_DATA
                            val sd = Environment.getExternalStorageDirectory()
                            val toDelete = File(sd, baseDirectory + documentItem.filePath)
                            toDelete.delete()
                            viewModel.deleteDoc(documentItem)
                        }
                        mode.finish()
                        true
                    }


                    else -> false
                }
            }

            override fun onDestroyActionMode(mode: ActionMode) {
                multiSelect = false
                selectedItems.clear()
                notifyDataSetChanged()
            }
        }



    inner class DocumentViewHolder(
        itemView: View,
        actionModeCallbacks: ActionMode.Callback,
        adapter: DocumentAdapter
    ) :
        ViewHolder(itemView) {
        private val categoryIcon: ImageView
        private val textViewLabel: TextView
        private val textViewTime: TextView
        private val textViewCategory: TextView
        private val textPageCount: TextView
        private val itemLayout: RelativeLayout
        private val btnOCR: ImageButton
        private val actionModeCallbacks: ActionMode.Callback
        private val adapter: DocumentAdapter
        private var documnt: Document? = null
        private val categoryImageMap: MutableMap<String, Int> = HashMap()
        fun selectItem(item: Document?) {
            if (adapter.multiSelect) {
                if (adapter.selectedItems.contains(item)) {
                    adapter.selectedItems.remove(item)
                    itemLayout.setBackgroundColor(Color.WHITE)
                } else {
                    adapter.selectedItems.add(item!!)
                    itemLayout.setBackgroundResource(R.color.orange_light)
                }
            }
        }

        fun setDocument(document: Document) {
            documnt = document
            textViewLabel.setText(document.name)
            textViewTime.setText(document.scanned)
            textViewCategory.setText(document.category)
            if (document.pageCount > 1) {
                textPageCount.visibility = View.VISIBLE
                textPageCount.setText(
                    java.lang.String.valueOf(document.pageCount).toString() + " Pages"
                )
            } else {
                textPageCount.visibility = View.GONE
            }
            if (adapter.selectedItems.contains(document)) {
                itemLayout.setBackgroundColor(Color.LTGRAY)
            } else {
                itemLayout.setBackgroundColor(Color.WHITE)
            }
            val resourceId = categoryImageMap[document.category]
            if (resourceId == null) {
                categoryIcon.setImageResource(R.drawable.ic_category_others)
            } else {
                categoryIcon.setImageResource(resourceId)
            }
            itemLayout.setOnClickListener { v ->
                if (adapter.multiSelect) {
                    selectItem(document)
                    if (adapter.selectedItems.size == 0) {
                        adapter.mActionMode!!.finish()
                    } else {
                        adapter.mActionMode!!.invalidate()
                    }
                } else {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    val sd: File = Environment.getExternalStorageDirectory()
                    val baseDirectory = BASE_STORAGE_DATA
                    val newFileName = baseDirectory + document.filePath
                    val toOpen = File(sd, newFileName)
                    val sharedFileUri: Uri = FileProvider.getUriForFile(
                        v.context,
                        "com.commercecx.cxscan.fileprovider",
                        toOpen
                    )
                    intent.setDataAndType(sharedFileUri, "application/pdf")
                    val pm = v.context.packageManager
                    if (intent.resolveActivity(pm) != null) {
                        v.context.startActivity(intent)
                    }
                }
            }
            itemView.setOnLongClickListener { view ->
                adapter.mActionMode =
                    (view.context as AppCompatActivity).startSupportActionMode(
                        actionModeCallbacks
                    )
                selectItem(document)
                true
            }

            btnOCR.setOnClickListener{
                val sd: File = Environment.getExternalStorageDirectory()
                val baseDirectory = BASE_STORAGE_DATA
                val newFileName = baseDirectory + document.filePath
                val file = File(sd, newFileName)
                doclistFrag!!.onOCRClick(file)
            }
        }

        init {
            categoryIcon = itemView.findViewById(R.id.imageView)
            textViewLabel = itemView.findViewById(R.id.fileName)
            textViewTime = itemView.findViewById(R.id.timeLabel)
            textViewCategory = itemView.findViewById(R.id.categoryLabel)
            textPageCount = itemView.findViewById(R.id.pageCount)
            itemLayout = itemView.findViewById(R.id.relativeLayout)
            btnOCR = itemView.findViewById(R.id.btnOCR)
            this.adapter = adapter
            this.actionModeCallbacks = actionModeCallbacks
            categoryImageMap["Others"] = R.drawable.ic_category_others
//            categoryImageMap["Shopping"] = R.drawable.ic_category_shopping
//            categoryImageMap["Vehicle"] = R.drawable.ic_category_vehicle
//            categoryImageMap["Medical"] = R.drawable.ic_category_medical
//            categoryImageMap["Legal"] = R.drawable.ic_category_legal
//            categoryImageMap["Housing"] = R.drawable.ic_category_housing
//            categoryImageMap["Books"] = R.drawable.ic_category_books
//            categoryImageMap["Food"] = R.drawable.ic_category_food
//            categoryImageMap["Banking"] = R.drawable.ic_category_banking
//            categoryImageMap["Receipts"] = R.drawable.ic_category_receipt
//            categoryImageMap["Manuals"] = R.drawable.ic_category_manuals
//            categoryImageMap["Travel"] = R.drawable.ic_category_travel
//            categoryImageMap["Notes"] = R.drawable.ic_category_notes
//            categoryImageMap["ID"] = R.drawable.ic_category_id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem = layoutInflater.inflate(R.layout.document_item_view, parent, false)

        return DocumentViewHolder(listItem, actionModeCallbacks, this)
    }

    override fun getItemCount(): Int {
        return documentList.size
    }

    override fun onBindViewHolder(holder: DocumentViewHolder, position: Int) {
        holder.setDocument( this.documentList[position] );
    }


    fun setData(documents: MutableList<Document>?) {
        documentList.clear()
        documentList.addAll(documents!!)
        notifyDataSetChanged()
    }
}