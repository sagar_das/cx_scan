package com.commercecx.cxscan.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.graphics.pdf.PdfDocument
import android.graphics.pdf.PdfDocument.PageInfo
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class PDFWriter {

    val document = PdfDocument()

    fun addFile(bitmapFile: File)
    {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        val bitmap = BitmapFactory.decodeFile(bitmapFile.absolutePath,options)
        val pageInfo = PageInfo.Builder(bitmap.width, bitmap.height, 1).create()
        val page = document.startPage(pageInfo)

        val canvas = page.canvas
        val paint = Paint()
        paint.setColor(Color.parseColor("#FFFFFF"))
        canvas.drawPaint(paint)

        canvas.drawBitmap(bitmap,0.0f,0.0f,null)
        document.finishPage(page)

    }

    fun addBitmap(bitmap: Bitmap){
        val pageInfo = PageInfo.Builder(bitmap.width, bitmap.height, 1).create()
        val page = document.startPage(pageInfo)

        val canvas = page.canvas
        val paint = Paint()
        paint.color = Color.parseColor("#ffffff")
        canvas.drawPaint(paint)

        canvas.drawBitmap(bitmap, 0.0f, 0.0f, null)
        document.finishPage(page)
    }

    @Throws(IOException::class)
    fun write(out: FileOutputStream?) {
        document.writeTo(out)
    }

    fun getPageCount(): Int {
        return document.pages.size
    }

    @Throws(IOException::class)
    fun close() {
        document.close()
    }


}