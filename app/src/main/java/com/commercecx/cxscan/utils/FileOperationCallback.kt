package com.commercecx.cxscan.utils

import java.io.FileOutputStream

interface FileOperationCallback {
    fun write(out: FileOutputStream)
}