package com.pengke.paper.scanner.crop

import android.widget.ImageView
import com.commercecx.cxscan.views.ScanRectangle



/**
 * Created by pengke on 15/09/2017.
 */
class ICropView {
    interface PaperDetectionImpl {
        fun getPaper(): ImageView
        fun getPaperRect(): ScanRectangle
        fun getCroppedPaper(): ImageView
    }
}