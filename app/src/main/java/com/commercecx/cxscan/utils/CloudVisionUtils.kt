package com.commercecx.cxscan.utils

//import com.google.android.gms.auth.GoogleAuthException
//import com.google.android.gms.auth.GoogleAuthUtil
//import com.google.android.gms.auth.UserRecoverableAuthException

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.util.Log
import android.widget.Toast
import com.commercecx.cxscan.BuildConfig
import com.commercecx.cxscan.fragment.DocumentListFragment
import com.commercecx.cxscan.fragment.OCRFragment
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.vision.v1.Vision
import com.google.api.services.vision.v1.VisionRequest
import com.google.api.services.vision.v1.VisionRequestInitializer
import com.google.api.services.vision.v1.model.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*


class CloudVisionUtils {

companion object{

    val CLOUD_VISION_API_KEY = BuildConfig.API_KEY
    val ANDROID_CERT_HEADER = "X-Android-Cert"
    val ANDROID_PACKAGE_HEADER = "X-Android-Package"
    val MAX_TEXT_RESULTS = 10
    val MAX_DIMENSION = 1200
    val TAG = this.javaClass.simpleName

    suspend fun connectToCloudVision(ocrFragment: OCRFragment, context: Context, annotate: Vision.Images.Annotate ) {


        var result: String? = null

    withContext(Dispatchers.Default) {
        try {
            Log.d(TAG, "created Cloud Vision request object, sending request")
            val response: BatchAnnotateImagesResponse = annotate.execute()
            result = convertResponseToString(response)
        } catch (e: GoogleJsonResponseException) {
            Log.d(TAG, "failed to make API request because " + e.content)
        } catch (e: IOException) {
            Log.d(
                TAG, "failed to make API request because of other IOException " +
                        e.message
            )
        }
        if(result == null) {
            result = "Cloud Vision API request failed. Check logs for details."
        }
    }

        withContext(Dispatchers.Main){
            //Toast.makeText(context,result,Toast.LENGTH_LONG).show()
            ocrFragment.setResultText(result!!)
        }



}

    @Throws(IOException::class)
     fun prepareAnnotationRequest(bitmap: Bitmap, packageName: String, packageManager: PackageManager): Vision.Images.Annotate? {
        val httpTransport = AndroidHttp.newCompatibleTransport()
        val jsonFactory: JsonFactory = GsonFactory.getDefaultInstance()
        val requestInitializer: VisionRequestInitializer =
            object : VisionRequestInitializer(CLOUD_VISION_API_KEY) {
                /**
                 * We override this so we can inject important identifying fields into the HTTP
                 * headers. This enables use of a restricted cloud platform API key.
                 */
                @Throws(IOException::class)
                override fun initializeVisionRequest(visionRequest: VisionRequest<*>) {
                    super.initializeVisionRequest(visionRequest)
                    visionRequest.requestHeaders[ANDROID_PACKAGE_HEADER] = packageName
                    val signature: String? =
                        PackageManagerUtils.getSignature(packageManager, packageName)
                    visionRequest.requestHeaders[ANDROID_CERT_HEADER] = signature
                }
            }
        val builder = Vision.Builder(httpTransport, jsonFactory, null)
        builder.setVisionRequestInitializer(requestInitializer)
        val vision = builder.build()
        val batchAnnotateImagesRequest = BatchAnnotateImagesRequest()
        batchAnnotateImagesRequest.requests = object : ArrayList<AnnotateImageRequest?>() {
            init {
                val annotateImageRequest = AnnotateImageRequest()

                // Add the image
                val base64EncodedImage = Image()
                // Convert the bitmap to a JPEG
                // Just in case it's a format that Android understands but Cloud Vision
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream)
                val imageBytes: ByteArray = byteArrayOutputStream.toByteArray()

                // Base64 encode the JPEG
                base64EncodedImage.encodeContent(imageBytes)
                annotateImageRequest.image = base64EncodedImage

                // add the features we want
                annotateImageRequest.features = object : ArrayList<Feature?>() {
                    init {
                        val textDetection = Feature()
                        textDetection.setType("TEXT_DETECTION")
                        textDetection.setMaxResults(MAX_TEXT_RESULTS)
                        add(textDetection)
                    }
                }

                // Add the list of one thing to the request
                add(annotateImageRequest)
            }
        }
        val annotateRequest =
            vision.images().annotate(batchAnnotateImagesRequest)
        // Due to a bug: requests to Vision API containing large images fail when GZipped.
        annotateRequest.disableGZipContent = true
        Log.d(TAG, "created Cloud Vision request object, sending request")
        return annotateRequest
    }

    private fun convertResponseToString(response: BatchAnnotateImagesResponse): String? {
        val message = StringBuilder("")

        val textList =
            response.responses[0].textAnnotations
        if (textList != null) {
            for (text in textList) {
//                message.append(
//                    String.format(
//                        Locale.US,
//                        "%.3f: %s",
//                        text.score,
//                        text.description
//                    )
//                )

                message.append(text.description)
                message.append("\n")
            }
        } else {
            message.append("nothing")
        }
        return message.toString()
    }

}
}