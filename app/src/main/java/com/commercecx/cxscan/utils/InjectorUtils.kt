package com.commercecx.cxscan.utils

import android.content.Context
import androidx.fragment.app.Fragment
import com.commercecx.cxscan.model.ScanDatabase
import com.commercecx.cxscan.model.instances.DocumentInstance
import com.commercecx.cxscan.viewmodel.DocumentViewModelFactory

object InjectorUtils {

    private fun getDocumentInstance(context: Context): DocumentInstance {
        return DocumentInstance.getInstance(ScanDatabase.getInstance(context.applicationContext).documentDao())
    }


    fun provideDocumentViewModelFactory(
        context: Context
    ): DocumentViewModelFactory {
        val documentInstance = getDocumentInstance(context)
        return DocumentViewModelFactory(documentInstance)
    }


}
