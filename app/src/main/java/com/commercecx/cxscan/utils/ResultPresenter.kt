package com.pengke.paper.scanner.crop

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.View
import com.commercecx.cxscan.utils.*
import com.commercecx.cxscan.utils.UriUtils.Companion.getUri
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.opencv.android.Utils
import org.opencv.core.Mat


const val IMAGES_DIR = "smart_scanner"

class ResultPresenter(val context: Context, val activity: Activity, private val iCropView: ICropView.PaperDetectionImpl) {
    private val picture: Mat? = SourceManager.pic

    private val corners: Corners? = SourceManager.corners
    private var croppedPicture: Mat? = null
    private var enhancedBitmap: Bitmap? = null
    private var croppedBitmap: Bitmap? = null



    init {
        iCropView.getPaperRect().onCorners2Crop(corners, picture?.size())
        val bitmap = Bitmap.createBitmap(picture?.width() ?: 1080, picture?.height()
                ?: 1920, Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(picture, bitmap, true)
        iCropView.getPaper().setImageBitmap(bitmap)
    }

    fun addImageToGallery(filePath: String, context: Context) {

        val values = ContentValues()

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
        values.put(MediaStore.MediaColumns.DATA, filePath)

        context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
    }

    fun crop() {
        if (picture == null) {
            Log.i(TAG, "picture null?")
            return
        }

        if (croppedBitmap != null) {
            Log.i(TAG, "already cropped")
            return
        }

        Observable.create<Mat> {
            it.onNext(cropPicture(picture, iCropView.getPaperRect().getCorners2Crop()))
        }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { pc ->
                    Log.i(TAG, "cropped picture: " + pc.toString())
                    croppedPicture = pc
                    croppedBitmap = Bitmap.createBitmap(pc.width(), pc.height(), Bitmap.Config.ARGB_8888)
                    Utils.matToBitmap(pc, croppedBitmap)
                    iCropView.getCroppedPaper().setImageBitmap(croppedBitmap)
                    iCropView.getPaper().visibility = View.GONE
                    iCropView.getPaperRect().visibility = View.GONE
                }
    }

    fun rotateRightBitmap(){

        if(croppedBitmap == null){
            Log.i(TAG, "picture null?")
            return
        }

        Observable.create<Bitmap>{
            it.onNext(rotateRight(croppedBitmap!!))
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                bitmap ->
                iCropView.getCroppedPaper().setImageBitmap(bitmap)
            }

    }
    fun rotateLeftBitmap(){

        if(croppedBitmap == null){
            Log.i(TAG, "picture null?")
            return
        }

        Observable.create<Bitmap>{
            it.onNext(rotateLeft(croppedBitmap!!))
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                    bitmap ->
                iCropView.getCroppedPaper().setImageBitmap(bitmap)
            }

    }

    fun enhance() {
        if (croppedBitmap == null) {
            Log.i(TAG, "picture null?")
            return
        }

        Observable.create<Bitmap> {
            it.onNext(enhancePicture(croppedBitmap))
        }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { pc ->
                    enhancedBitmap = pc
                    iCropView.getCroppedPaper().setImageBitmap(pc)
                }
    }

    suspend fun save() {
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(context, "please grant write file permission and trya gain", Toast.LENGTH_SHORT).show()
//        } else {
//            val dir = File(Environment.getExternalStorageDirectory(), IMAGES_DIR)
//            if (!dir.exists()) {
//                dir.mkdirs()
//            }

        //first save enhanced picture, if picture is not enhanced, save cropped picture, otherwise nothing to do
//            val pic = enhancedBitmap
//            if (pic != null) {
//                val file = File(dir, "enhance_${SystemClock.currentThreadTimeMillis()}.jpeg")
//                val outStream = FileOutputStream(file)
//                pic.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
//                outStream.flush()
//                outStream.close()
//                addImageToGallery(file.absolutePath, this.context)
//                Toast.makeText(context, "picture saved, path: ${file.absolutePath}", Toast.LENGTH_SHORT).show()
//            } else {
//                val cropPic = croppedBitmap
//                if (cropPic != null) {
//                    val file = File(dir, "crop_${SystemClock.currentThreadTimeMillis()}.jpeg")
//                    val outStream = FileOutputStream(file)
//                    cropPic.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
//                    outStream.flush()
//                    outStream.close()
//                    addImageToGallery(file.absolutePath, this.context)
//                    Toast.makeText(context, "picture saved, path: ${file.absolutePath}", Toast.LENGTH_SHORT).show()
//                }
//            }


        try {
            withContext(Dispatchers.Default) {
                val data = Intent()
                var bitmap: Bitmap? = enhancedBitmap
                if (bitmap == null) {
                    bitmap = croppedBitmap
                }
                val uri: Uri? = getUri(activity, bitmap!!)
                data.putExtra(SCANNED_RESULT, uri)
                data.putExtra(SCAN_MORE, false)
                activity.setResult(Activity.RESULT_OK, data)
                enhancedBitmap!!.recycle()
                croppedBitmap!!.recycle()
                System.gc()
                withContext(Dispatchers.Main) {
                    activity.finish()
                }

            }
        }catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }






