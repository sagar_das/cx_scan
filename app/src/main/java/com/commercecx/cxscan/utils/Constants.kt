package com.commercecx.cxscan.utils

import android.os.Environment

const val DATABASE_NAME = "cxscan-database"
const val PERMISSIONS_REQ_CODE = 101
const val BASE_STORAGE_DATA = "cx_scanner/data/"
const val BASE_STAGING_DATA = "cx_scanner/stage/"
const val PICKFILE_REQUEST_CODE = 1
const val START_CAMERA_REQUEST_CODE = 2
const val OPEN_INTENT_PREFERENCE = "selectContent"
const val IMAGE_BASE_PATH_EXTRA = "ImageBasePath"
const val OPEN_CAMERA = 4
const val OPEN_MEDIA = 5
const val SCANNED_RESULT = "scannedResult"
const val PDF_ARG_KEY = "PDF_FILE"
val IMAGE_PATH: String =
    Environment.getExternalStorageDirectory().getPath().toString() + "/cx_scanner/tmp"

const val SELECTED_BITMAP = "selectedBitmap"
const val SCAN_MORE = "scanMore"

