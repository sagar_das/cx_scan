package com.pengke.paper.scanner.scan

import android.view.Display
import android.view.SurfaceView
import com.commercecx.cxscan.views.ScanRectangle


interface IScanView {
    interface Proxy {
        fun exit()
        fun getDisplay(): Display
        fun getSurfaceView(): SurfaceView
        fun getPaperRect(): ScanRectangle
    }
}