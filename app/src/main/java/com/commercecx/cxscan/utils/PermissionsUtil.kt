package com.commercecx.cxscan.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity


class PermissionsUtil() {
//    private fun checkCameraPerission(): Boolean {
//        // Permission is not granted
//        return ContextCompat.checkSelfPermission(
//            BaseApplication.getApplicationContext(),
//            Manifest.permission.CAMERA
//        ) === PackageManager.PERMISSION_GRANTED
//    }



    companion object{

        fun hasPermissions(context: Context): Boolean  {
            return ContextCompat.checkSelfPermission(context,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context,Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context,Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context,Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED
        }

        fun requestPermissions(activity: FragmentActivity) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(
                    activity,
                    Manifest.permission.CAMERA
                )
            ) {
                ActivityCompat.requestPermissions(
                    activity, arrayOf(Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.GET_ACCOUNTS

                    ),
                    PERMISSIONS_REQ_CODE
                )
            }
        }
    }
}