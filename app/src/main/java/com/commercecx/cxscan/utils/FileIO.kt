package com.commercecx.cxscan.utils

import android.os.Environment
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class FileIO {

    companion object {
        fun getAllFiles(dirPath: String): List<File> {
            val fileList: MutableList<File> = mutableListOf()
            val storageDirectory = Environment.getExternalStorageDirectory()
            val targetDirectory = File(storageDirectory, dirPath)

            if (targetDirectory.listFiles() != null) {
                for (eachFile in targetDirectory.listFiles()) {
                    fileList.add(eachFile)
                }
            }

            return fileList
        }

        fun mkdir(dirPath: String?) {
            val sd = Environment.getExternalStorageDirectory()
            val storageDirectory = File(sd, dirPath)
            if (!storageDirectory.exists()) {
                storageDirectory.mkdir()
            }
        }

        fun clearDirectory(dirPath: String?) {
            val sd = Environment.getExternalStorageDirectory()
            val targetDirectory = File(sd, dirPath)
            if (targetDirectory.listFiles() != null) {
                for (tempFile in targetDirectory.listFiles()) {
                    tempFile.delete()
                }
            }
        }

        @Throws(IOException::class)
        fun writeFile(
            baseDirectory: String,
            filename: String,
            callback: FileOperationCallback
        ) {
            val sd = Environment.getExternalStorageDirectory()
            val absFilename = baseDirectory + filename
            val dest = File(sd, absFilename)
            val out = FileOutputStream(dest)
            callback.write(out)
            out.flush()
            out.close()
        }
    }
}