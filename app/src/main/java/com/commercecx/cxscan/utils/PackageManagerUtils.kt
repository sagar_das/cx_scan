package com.commercecx.cxscan.utils

import android.content.pm.PackageManager
import android.content.pm.Signature
import androidx.annotation.NonNull
import com.google.common.io.BaseEncoding
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class PackageManagerUtils {

    companion object {
        fun getSignature(
            @NonNull pm: PackageManager,
            @NonNull packageName: String
        ): String? {
            return try {
                val packageInfo =
                    pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
                if (packageInfo == null || packageInfo.signatures == null || packageInfo.signatures.size == 0 || packageInfo.signatures[0] == null
                ) {
                    null
                } else signatureDigest(packageInfo.signatures[0])
            } catch (e: PackageManager.NameNotFoundException) {
                null
            }
        }

        private fun signatureDigest(sig: Signature): String? {
            val signature: ByteArray = sig.toByteArray()
            return try {
                val md: MessageDigest = MessageDigest.getInstance("SHA1")
                val digest: ByteArray = md.digest(signature)
                BaseEncoding.base16().lowerCase().encode(digest)
            } catch (e: NoSuchAlgorithmException) {
                null
            }
        }
    }
}