package com.commercecx.cxscan.model.instances

import com.commercecx.cxscan.model.Document
import com.commercecx.cxscan.model.dao.DocumentDao

/* Singleton Class */

class DocumentInstance private constructor(private val documentDao: DocumentDao){

    fun getAllDocs() = documentDao.getAllDocs()
    fun getDocsByName(nameKey : String) = documentDao.getDocsByName(nameKey)
    fun getDocsById(idKey: Int) = documentDao.getDocById(idKey)
    suspend fun insertDoc(document: Document) {
        documentDao.insertDoc(document)
    }
    suspend fun insertAllDoc(documents: List<Document>) {
        documentDao.insertAllDocs(documents)
    }
    suspend fun updateDoc(document: Document) {
        documentDao.updateDoc(document)
    }
    suspend fun deleteDoc(document: Document) {
        documentDao.deleteDoc(document)
    }

    companion object{

        @Volatile private var instance : DocumentInstance? = null

        fun getInstance(documentDao: DocumentDao) =

            instance?: synchronized(this){
                instance?: DocumentInstance(documentDao).also { instance = it}
            }

    }
}