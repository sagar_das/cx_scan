package com.commercecx.cxscan.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.commercecx.cxscan.model.dao.DocumentDao
import com.commercecx.cxscan.utils.DATABASE_NAME

@Database(entities = [Document::class], version = 1, exportSchema = false)
abstract class ScanDatabase : RoomDatabase() {

    abstract fun documentDao(): DocumentDao

    //For singleton instance

    companion object{
        @Volatile private var instance: ScanDatabase? = null

        fun getInstance(context: Context): ScanDatabase {
            return instance?: synchronized(this) {
                instance?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context) : ScanDatabase {

            return Room.databaseBuilder(context, ScanDatabase::class.java, DATABASE_NAME)
                .addCallback(object : RoomDatabase.Callback(){
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                    }
                }).allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}