package com.commercecx.cxscan.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "documents")
data class Document(
    var name:String,
    var category: String,
    var filePath: String,
    var pageCount: Int,
    var scanned: String

) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var documentId: Int = 0
}