package com.commercecx.cxscan.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.commercecx.cxscan.model.Document

@Dao
interface DocumentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllDocs (documents: List<Document>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDoc(document: Document)

    @Query("SELECT * FROM documents ORDER BY id DESC")
    fun getAllDocs(): LiveData<List<Document>>

    @Query("SELECT * FROM documents WHERE name like :nameKey ORDER BY id DESC")
    fun getDocsByName(nameKey: String): LiveData<List<Document>>

    @Query("SELECT * FROM documents WHERE id like :idKey")
    fun getDocById(idKey:Int): Document

    @Update
    suspend fun updateDoc(document: Document)

    @Delete
    suspend fun deleteDoc(document: Document)




}